package me.neon.nlib.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class BlockGround extends BlockItemdrop {

	private EnumPlantType[] plt;
	
	public BlockGround(String uname, Material mat, Item[] drop, int amt, int rand, EnumPlantType[] plantable) {
		super(uname, mat, drop, amt, rand);
		this.plt = plantable;
	}
	
	public BlockGround(String uname, Material mat, Item[] drop, int min, int max, int rand, EnumPlantType[] plantable) {
		super(uname, mat, drop, min, max, rand);
		this.plt = plantable;
	}
	
	public BlockGround(String uname, Material mat, EnumPlantType[] plantable) {
		super(uname, mat);
		this.plt = plantable;
	}

	@Override
	public boolean canSustainPlant(IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable) {
		super.canSustainPlant(world, pos, direction, plantable);
		//IBlockState plant = plantable.getPlant(world, new BlockPos(pos.getX(), pos.getY() +1, pos.getZ()));
        EnumPlantType plantType = plantable.getPlantType(world, new BlockPos(pos.getX(), pos.getY() +1, pos.getZ()));
        
		if(plt.length != 0){
			for(int i=0;i<plt.length;i++){
				if(plantType.equals(plt[i])){
					return true;
				}
			}
		}
		
		return false;
	}

}
