package me.neon.nlib.block;

import me.neon.nlib.api.INamable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class NBlock extends Block implements INamable{

	private String name;
	
	public NBlock(String name, Material mat){
		super(mat);
		this.name = name;
		this.setUnlocalizedName(name);
	}
	
	public String getName(){
		return name;
	}
	
}
