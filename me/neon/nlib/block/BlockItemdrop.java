package me.neon.nlib.block;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockItemdrop extends NBlock {

	private Item[] drop;
	private int min;
	private int max;
	private int rand;

	public BlockItemdrop(String uname, Material mat, Item[] drop, int min, int max, int rand) {
		super(uname, mat);
		this.drop = drop;
		this.min = min;
		this.max = max;
		this.rand = rand;
	}

	public BlockItemdrop(String uname, Material mat, Item[] drop, int amt, int rand) {
		super(uname, mat);
		this.drop = drop;
		this.min = amt;
		this.max = amt;
		this.rand = rand;
	}

	public BlockItemdrop(String uname, Material mat) {
		super(uname, mat);
		this.rand = -1;
	}

	@Override
	public Item getItemDropped(IBlockState state, Random random, int fortune) {
		if (drop.length == 1) {
			return drop[drop.length - 1];
		} else if (rand == -1) {
			return super.getItemDropped(state, random, fortune);
		} else {
			return drop[(random.nextInt(rand - drop.length) + drop.length) - 1];
		}
	}

	@Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune){
		if (rand == -1) {
			return super.getDrops(world, pos, state, fortune);
		} else {
			ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
			Random r = new Random();
			int ammount = this.quantityDropped(state, fortune, r);
			for (int i = 0; i < ammount; i++) {
				Item item = getItemDropped(state, new Random(), fortune);
				if (item != null) {
					ret.add(new ItemStack(item, 1, damageDropped(state)));
				}
			}
			return ret;
		}
	}

	@Override
	public int quantityDropped(IBlockState state, int fortune, Random random) {
		fortune += 1;
		int r = random.nextInt(rand) == drop.length ? (random.nextInt(max - min) + max * fortune) : (random.nextInt(max - min) + min * fortune);
		return r;
	}

}
