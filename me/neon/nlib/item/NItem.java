package me.neon.nlib.item;

import me.neon.nlib.api.INamable;
import net.minecraft.item.Item;

public class NItem extends Item implements INamable{

	private String name;
	
	public NItem(String name){
		this.name = name;
		setUnlocalizedName(name);
	}
	
	public String getName(){
		return name;
	}
}
