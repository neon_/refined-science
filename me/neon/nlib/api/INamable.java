package me.neon.nlib.api;

public interface INamable {

	public String getName();
	
}
