package me.neon.nlib.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.tileentity.TileEntity;

public abstract class NTileEntity extends TileEntity implements IUpdatePlayerListBox {

    public void readFromNBT(NBTTagCompound nbt){
    	super.readFromNBT(nbt);
    }
    
    public void writeToNBT(NBTTagCompound nbt){
    	super.writeToNBT(nbt);
    }
    
    public Packet getDescriptionPacket() {
		NBTTagCompound nbtTag = new NBTTagCompound();
		this.writeToNBT(nbtTag);
		return new S35PacketUpdateTileEntity(pos, 1, nbtTag);
	}

	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity  packet) {
		this.readFromNBT(packet.getNbtCompound());
	}
	
}
