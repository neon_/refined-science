package me.neon.nlib.tile;

import java.util.ArrayList;

import net.minecraft.inventory.ISidedInventory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;

public abstract class NMachine extends NTileEntity implements ISidedInventory {

	protected String tileName;
	private ArrayList<String> canAccess = new ArrayList<String>();

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		if (nbt.getString("CustomName") != null) {
			tileName = nbt.getString("CustomName");
		}
		NBTTagList list = nbt.getTagList("AccessList", Constants.NBT.TAG_COMPOUND);
		for (int i = 0; i < list.tagCount(); i++) {
			NBTTagCompound tag = list.getCompoundTagAt(i);
			String user = tag.getString("User_"+i);
			canAccess.add(i, user);
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		if (tileName != null) {
			nbt.setString(tileName, "CustomName");
		}
		NBTTagList list = new NBTTagList();
		for (int i = 0; i < canAccess.size(); i++) {
			String user = canAccess.get(i);
			if (user != null) {
				NBTTagCompound tag = new NBTTagCompound();
				tag.setString("User_" + i, user);
				list.appendTag(tag);
			}
		}
		nbt.setTag("AccessList", list);
	}

	public void setName(String name) {
		this.tileName = name;
	}

	public boolean isNameLocalized() {
		return (tileName == null) ? false : true;
	}

	public ArrayList<String> getUsers() {
		return this.canAccess;
	}

	public void setOwner(String owner) {
		this.canAccess.add(0, owner);;
	}

	public void addUser(String user) {
		this.canAccess.add(user);
	}

	public void removeUser(String user) {
		this.canAccess.remove(user);
	}

	public String getUser(int index) {
		return this.canAccess.get(index);
	}

	public boolean hasUser(String user) {
		return this.canAccess.contains(user);
	}

	public String getOwner() {
		return this.canAccess.get(0);
	}

}
