package me.neon.refinedscience.lib.handle;

import me.neon.refinedscience.worldgen.biome.BiomeClaystoneDesert;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.BiomeManager.BiomeEntry;
import net.minecraftforge.common.BiomeManager.BiomeType;

public class BiomeHandler {

	public static void init(){
		defineBiomes();
		registerBiomes();
	}
	
	public static BiomeGenBase claystoneDesert;

	public static void defineBiomes(){
    	claystoneDesert = new BiomeClaystoneDesert(138);
	}
	
	public static void registerBiomes(){
      	BiomeDictionary.registerBiomeType(claystoneDesert, Type.WASTELAND, Type.HOT, Type.SPARSE, Type.SANDY);
        BiomeManager.addBiome(BiomeType.DESERT, new BiomeEntry(claystoneDesert, 25));
        BiomeManager.addSpawnBiome(claystoneDesert);
	}
}
