package me.neon.refinedscience.lib.handle;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class ConfigHandler {

	private static Configuration cfg;
	
    public static void init(File configFile) {
    	cfg = new Configuration(configFile);
    	cfg.load();
    }
    
    public static Configuration getConfig(){
		return cfg;
    }
    
    public static void addDefaultValues(){
    	cfg.load();
    	cfg.addCustomCategoryComment("Display", "On-screen display, renders,...");
    	cfg.get("Display", "overlay.x", 64);
    	cfg.get("Display", "overlay.y", 64);
    	cfg.save();
    }
}
