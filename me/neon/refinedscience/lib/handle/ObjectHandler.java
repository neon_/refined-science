package me.neon.refinedscience.lib.handle;

import java.util.ArrayList;

import me.neon.nlib.api.INamable;
import me.neon.nlib.block.BlockGround;
import me.neon.nlib.block.NBlock;
import me.neon.nlib.item.NItem;
import me.neon.refinedscience.block.machine.BlockFluidPipe;
import me.neon.refinedscience.item.ItemDebug;
import me.neon.refinedscience.lib.CustomTabs;
import me.neon.refinedscience.lib.IdList;
import me.neon.refinedscience.tile.TilePipe;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ObjectHandler {

	private static ArrayList<String> tiles = new ArrayList<String>();
	private static ArrayList<String> items = new ArrayList<String>();
	
	public static void registerItem(Item nItem) {
		GameRegistry.registerItem(nItem, ((INamable) nItem).getName());
		items.add(((INamable) nItem).getName());
	}
	
	@SideOnly(Side.CLIENT)
	public static void addItemModel(Item nItem){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(nItem, 0, new ModelResourceLocation(IdList.MOD_ID+":"+((INamable) nItem).getName(), "inventory"));
	}
	
	public static void registerBlock(Block nTile) {
		GameRegistry.registerBlock(nTile, ((INamable) nTile).getName());
		tiles.add(((INamable) nTile).getName());
		addBlockModel(nTile);
	}
	
	@SideOnly(Side.CLIENT)
	public static void addBlockModel(Block nTile){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(nTile), 0, new ModelResourceLocation(IdList.MOD_ID+":"+((INamable) nTile).getName(), "inventory"));
	}
	
	/*/////////////////////////////////////////
	/* BLOCK START HERE ///////////////////////
	/*/////////////////////////////////////////
	
	public static Block glassFluidPipe;
	public static Block clayRed;
	public static Block stoneClay;
	public static Block brickClay;
	//public static Block blockTest;
		
	public static void initBlocks(){
		clayRed = new BlockGround("clayRed", Material.clay, new Item[]{clayballRed}, 3, 5, 5, new EnumPlantType[]{EnumPlantType.Beach, EnumPlantType.Desert}).setCreativeTab(CustomTabs.tabGeneration).setResistance(13F).setHardness(0.8F).setStepSound(Block.soundTypeGravel);
		stoneClay = new NBlock("stoneClay", Material.ground).setCreativeTab(CustomTabs.tabGeneration).setResistance(15F).setHardness(3F).setStepSound(Block.soundTypeStone);
		//brickClay = new NBlock("brickClay", Material.ground).setCreativeTab(CustomTabs.tabGeneration).setResistance(20F).setHardness(3.5F).setStepSound(Block.soundTypeStone);
		glassFluidPipe = new BlockFluidPipe("glassPipe", Material.glass).setCreativeTab(CustomTabs.tabFunction).setResistance(3F).setHardness(1F).setStepSound(Block.soundTypeGlass);
		//blockTest = new BlockConnected("blockTest", Material.ground).setCreativeTab(CustomTabs.tabGeneration);
	}
	
	public static void registerBlocks(){
		registerBlock(clayRed);
		registerBlock(stoneClay);
		//registerBlock(s, brickClay);
		registerBlock(glassFluidPipe);
		//ObjectHelper.registerBlock(blockTest);
	}
	
	public static void addBlockProperties(){
		clayRed.setHarvestLevel("shovel", 0);
		stoneClay.setHarvestLevel("pickaxe", 1);
	}
	
	public static int getRBS(){
		return tiles.size();
	}
	
	public static ArrayList<String> getRBN(){
		return tiles;
	}

	
	/*/////////////////////////////////////////
	/* ITEM START HERE ////////////////////////
	/*/////////////////////////////////////////
	
	public static Item itemDebug;
	public static Item clayballRed;
	
	public static void initItems(){
		clayballRed = new NItem("clayballRed").setCreativeTab(CustomTabs.tabGeneration);
		itemDebug = new ItemDebug("itemDebug").setCreativeTab(CustomTabs.tabFunction);
	}
	
	public static void registerItems(){
		registerItem(clayballRed);
		registerItem(itemDebug);
	}
	
	public static int getRIS(){
		return items.size();
	}
	
	public static ArrayList<String> getRIN(){
		return items;
	}
	
	/*////////////////////////////////
	/* OTHER /////////////////////////
	/*////////////////////////////////
	
	public static void registerEvents(){
		
	}

	public static void mapTiles() {
		GameRegistry.registerTileEntity(TilePipe.class, "pipeGlass");
	}
	
	
}
