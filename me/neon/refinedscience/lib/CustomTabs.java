package me.neon.refinedscience.lib;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import me.neon.refinedscience.lib.handle.ObjectHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

public class CustomTabs {

	public static CreativeTabs tabFunction;
	public static CreativeTabs tabGeneration;

	public static void init() {
		tabFunction = new CreativeTabs(CreativeTabs.getNextID(), "function") {
			@SideOnly(Side.CLIENT)
			public Item getTabIconItem() {
				return ObjectHandler.clayballRed;
			}
		};
		tabGeneration = new CreativeTabs(CreativeTabs.getNextID(), "generation") {
			@SideOnly(Side.CLIENT)
			public Item getTabIconItem() {
				return ObjectHandler.clayballRed;
			}
		};
	}

}
