package me.neon.refinedscience.lib;

import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class IdList {

    public static final String MOD_ID = "RS";
    public static final String MOD_NAME = "Refined Science";
    public static final String MOD_VERSION = "1.9.5 Alpha";
    public static final String CLIENT_PROXY = "me.neon.refinedscience.network.ClientProxy";
    public static final String SERVER_PROXY = "me.neon.refinedscience.network.CommonProxy";
        
    public static String[] allElementNames = new String[] {
    	"Hidrogen", "Nitrogen", "Oxygen", "Flourene", "Chlorine", "Bromine", "Iodine",
    	"Helium", "Neon", "Argon", "Krypton", "Xenon", "Radon",
    	"Lithium", "Sodium", "Potassium", "Rubidium", "Caesium", "Francium",
    	"Beryllium", "Magnesium", "Calcium", "Strontium", "Barium", "Radium",
    	"Boron", "Silicon", "Germanium", "Arsenic", "Anotimony", "Tellurium", "Astatine",
    	"Carbon", "Phosphorus", "Sulfur", "Selenium",
    	"Aluminum", "Gallium", "Indium", "Tin", "Thallium", "Lead", "Bismuth", "Polonium",
    	"Scandium", "Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt", "Nickel", "Copper", "Zinc",
    	"Yttrium", "Zirconium", "Niobium", "Molybdenum", "Technetium", "Ruthenium", "Rhodium", "Palladium", "Silver", "Cadmium",
    	"Hafnium", "Tantalum", "Tungsten", "Rehnium", "Osmium", "Iridium", "Platinum", "Gold", "Mercury",
    	"Lanthanum", "Cerium", "Praseodymium", "Neodymium", "Promethium", "Samarium", "Europium", "Gadolinium", "Teribium", "Dysprosium", "Holmium", "Eribium", "Thulium", "Ytterbium", "Lutetium",
    	"Actinium", "Thorium", "Protactinium", "Uranium", "Neptunium", "Plutonium", "Americium", "Curium", "Berkelium", "Californium"
    };
    
    public static String[] allElementSubnames = new String[] {
    	"H", "N", "O", "F", "Cl", "Br", "I",
    	"He", "Ne", "Ar", "Kr", "Xe", "Rn",
    	"Li", "Na", "K", "Rb", "Cs", "Fr",
    	"Be", "Mg", "Ca", "Sr", "Ba", "Ra",
    	"B", "Si", "Ge", "As", "Sb", "Te", "At",
    	"C", "P", "S", "Se",
    	"Al", "Ga", "In", "Sn", "Tl", "Pb", "Bi", "Po",
    	"Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
    	"Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd",
    	"Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
    	"La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu",
    	"Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf"
    };
    
}
