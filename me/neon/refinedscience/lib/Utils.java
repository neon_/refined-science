package me.neon.refinedscience.lib;

import java.awt.Color;
import java.util.ArrayList;

import me.neon.refinedscience.lib.handle.ObjectHandler;
import me.neon.refinedscience.tile.TilePipe;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.StatCollector;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Utils {

	public static ArrayList<EnumFacing> getConnectedSides(IBlockAccess worldObj, IBlockState b, int x, int y, int z) {
		ArrayList<EnumFacing> al = new ArrayList<EnumFacing>();
		if (worldObj.getBlockState(new BlockPos(x, y + 1, z)).equals(b)) {
			al.add(EnumFacing.UP);
		}
		if (worldObj.getBlockState(new BlockPos(x, y - 1, z)).equals(b)) {
			al.add(EnumFacing.DOWN);
		}
		if (worldObj.getBlockState(new BlockPos(x + 1, y, z)).equals(b)) {
			al.add(EnumFacing.EAST);
		}
		if (worldObj.getBlockState(new BlockPos(x - 1, y, z)).equals(b)) {
			al.add(EnumFacing.WEST);
		}
		if (worldObj.getBlockState(new BlockPos(x, y, z + 1)).equals(b)) {
			al.add(EnumFacing.SOUTH);
		}
		if (worldObj.getBlockState(new BlockPos(x, y, z - 1)).equals(b)) {
			al.add(EnumFacing.NORTH);
		}
		return al;
	}

	public static ArrayList<EnumFacing> getPipeConnection(IBlockAccess worldObj, IBlockState b, BlockPos p) {
		ArrayList<EnumFacing> ret = getConnectedSides(worldObj, b, p.getX(), p.getY(), p.getZ());
		ArrayList<EnumFacing> rem = new ArrayList<EnumFacing>();
		TileEntity teb = worldObj.getTileEntity(p);
		for (int i = 0; i < ret.size(); i++) {
			TileEntity ne = worldObj.getTileEntity(new BlockPos(p.getX() + ret.get(i).getFrontOffsetX(), p.getY() + ret.get(i).getFrontOffsetY(), p.getZ() + ret.get(i).getFrontOffsetZ()));
			if (((TilePipe) ne).getFluidTank().getFluidAmount() > 0 && ((TilePipe) teb).getFluidTank().getFluidAmount() > 0) {
				if (!((TilePipe) ne).getFluidTank().getFluid().isFluidEqual(((TilePipe) teb).getFluidTank().getFluid())) {
					rem.add(ret.get(i));
				}
			}
			//TODO: FIX THIS
			/*if (!((TilePipe) teb).noConnect.isEmpty()) {
				if(((TilePipe) teb).noConnect.get(i) != null){
					if (!((TilePipe) ne).noConnect.contains(((TilePipe) teb).noConnect.get(i))) {
						((TilePipe) ne).noConnect.add(((TilePipe) teb).noConnect.get(i).getOpposite());
					}
					rem.add(((TilePipe) teb).noConnect.get(i));
				}
			}*/
		}
		return removeFromList(ret, rem);
	}

	public static ArrayList<EnumFacing> removeFromList(ArrayList<EnumFacing> main, ArrayList<EnumFacing> remove) {
		for (int r = 0; r < remove.size(); r++) {
			main.remove(remove.get(r));
		}
		return main;
	}

	public static ArrayList<EnumFacing> getAllConnectedSides(IBlockAccess worldObj, int x, int y, int z) {
		ArrayList<EnumFacing> al = new ArrayList<EnumFacing>();
		if (worldObj.getBlockState(new BlockPos(x, y + 1, z))!=null) {
			al.add(EnumFacing.UP);
		}
		if (worldObj.getBlockState(new BlockPos(x, y - 1, z))!=null) {
			al.add(EnumFacing.DOWN);
		}
		if (worldObj.getBlockState(new BlockPos(x + 1, y, z))!=null) {
			al.add(EnumFacing.EAST);
		}
		if (worldObj.getBlockState(new BlockPos(x - 1, y, z))!=null) {
			al.add(EnumFacing.WEST);
		}
		if (worldObj.getBlockState(new BlockPos(x, y, z + 1))!=null) {
			al.add(EnumFacing.SOUTH);
		}
		if (worldObj.getBlockState(new BlockPos(x, y, z - 1))!=null) {
			al.add(EnumFacing.NORTH);
		}
		return al;
	}
	
	public static String unlocalizeResource(String raw){
		return IdList.MOD_ID+":"+raw;
	}
	
	public static ItemStack consumeItem(ItemStack stack) {
		if (stack.stackSize == 1) {
			if (stack.getItem().hasContainerItem(stack)) {
				return stack.getItem().getContainerItem(stack);
			} else {
				return null;
			}
		} else {
			stack.splitStack(1);
			return stack;
		}
	}
	
	public static String getHexName(Color color) {
		int r = color.getRed();
		int g = color.getGreen();
		int b = color.getBlue();

		String rHex = Integer.toString(r, 16);
		String gHex = Integer.toString(g, 16);
		String bHex = Integer.toString(b, 16);

		return (rHex.length() == 2 ? "" + rHex : "0" + rHex) + (gHex.length() == 2 ? "" + gHex : "0" + gHex) + (bHex.length() == 2 ? "" + bHex : "0" + bHex);
	}

	public static String blend(String hex1, String hex2, double ratio) {
		Color color1 = Color.getColor(hex1);
		Color color2 = Color.getColor(hex2);
		float r = (float) ratio;
		float ir = (float) 1.0 - r;

		float rgb1[] = new float[3];
		float rgb2[] = new float[3];

		color1.getColorComponents(rgb1);
		color2.getColorComponents(rgb2);

		Color color = new Color(rgb1[0] * r + rgb2[0] * ir, rgb1[1] * r + rgb2[1] * ir, rgb1[2] * r + rgb2[2] * ir);

		return getHexName(color);
	}
}