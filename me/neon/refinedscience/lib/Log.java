package me.neon.refinedscience.lib;

import java.io.File;
import java.util.Locale;

import net.minecraftforge.fml.relauncher.Side;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Log {
	
	public static Log log = new Log();
	
	static File minecraftHome;
	private static boolean configured;

	private Logger myLog;

	static Side side;

	private Log() {}

	private static void configureLogging() {
		log.myLog = LogManager.getLogger(IdList.MOD_ID);
		ThreadContext.put("side", side.name().toLowerCase(Locale.ENGLISH));
		configured = true;
	}

	public static void log(String targetLog, Level level, String format, Object... data) {
		LogManager.getLogger(targetLog).log(level, String.format(format, data));
	}

	public static void log(Level level, String format, Object... data) {
		if (!configured) {
			configureLogging();
		}
		log.myLog.log(level, String.format(format, data));
	}
	
	public Logger getLogger() {
		return myLog;
	}
	
    public static void log(Level logLevel, Object... msg) {
    	log.log(IdList.MOD_ID, logLevel, String.format("%s", msg));
    }
    
    public static void info(Object... msg){
    	log.log(IdList.MOD_ID, Level.INFO, String.format("%s", msg));
    }
    
    public static void err(Object... msg){
    	log.log(IdList.MOD_ID, Level.ERROR, String.format("%s", msg));
    }
	
}
