package me.neon.refinedscience;

import me.neon.refinedscience.lib.CustomTabs;
import me.neon.refinedscience.lib.IdList;
import me.neon.refinedscience.lib.Log;
import me.neon.refinedscience.lib.handle.BiomeHandler;
import me.neon.refinedscience.lib.handle.ConfigHandler;
import me.neon.refinedscience.lib.handle.ObjectHandler;
import me.neon.refinedscience.network.CommonProxy;
import me.neon.refinedscience.worldgen.biome.BiomeClaystoneDesert;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.BiomeManager.BiomeEntry;

import org.apache.logging.log4j.Level;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = IdList.MOD_ID, name = IdList.MOD_NAME, version = IdList.MOD_VERSION)
public class RefinedScience {
	
    @Instance(IdList.MOD_ID)
    public static RefinedScience instance = new RefinedScience();

    @SidedProxy(clientSide = IdList.CLIENT_PROXY, serverSide = IdList.SERVER_PROXY)
    public static CommonProxy proxy;
            
    @EventHandler
    public static void preInit(FMLPreInitializationEvent event) {
    	ObjectHandler.registerEvents();
    	Log.info("Setting up config file...");
    	ConfigHandler.init(event.getSuggestedConfigurationFile());
    	Log.info("Starting content registry.");
    	CustomTabs.init();
    }

    @EventHandler
    public static void init(FMLInitializationEvent event) {
    	Log.info("Initializing renderers...");
    	proxy.initRenderers();
    	Log.info("Initializing sounds...");
    	proxy.initSounds();
    	Log.info("Initilazing items...");
    	ObjectHandler.initItems();
    	ObjectHandler.registerItems();
    	Log.info("Registered "+ObjectHandler.getRIS()+" items.");
    	Log.info("Initilazing blocks...");
    	ObjectHandler.initBlocks();
    	ObjectHandler.registerBlocks();
    	Log.info("Registered "+ObjectHandler.getRBS()+" blocks.");
    	Log.info("Mapping tiles...");
    	ObjectHandler.mapTiles();
    	Log.info("Hooking events...");
    	ObjectHandler.registerEvents();
    }

    @EventHandler
    public static void postInit(FMLPostInitializationEvent event) {
    	Log.info("Adding biomes...");
    	BiomeHandler.init();
    	Log.info("Initization ended.");
    }
}
