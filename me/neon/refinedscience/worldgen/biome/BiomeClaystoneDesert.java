package me.neon.refinedscience.worldgen.biome;

import java.awt.Color;
import java.util.Random;

import me.neon.refinedscience.lib.handle.ObjectHandler;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;

public class BiomeClaystoneDesert extends BiomeGenBase {

	public BiomeClaystoneDesert(int id) {
		super(id, false);
		this.setBiomeName("Claystone Desert");
		this.theBiomeDecorator.treesPerChunk = 0;
		this.theBiomeDecorator.deadBushPerChunk = 6;
		this.theBiomeDecorator.reedsPerChunk = 3;
		this.theBiomeDecorator.cactiPerChunk = 4;
		this.theBiomeDecorator.generateLakes = true;
		this.waterColorMultiplier = 0xddd83c;
		this.spawnableMonsterList.clear();
		this.spawnableCreatureList.clear();
		this.topBlock = ObjectHandler.clayRed.getDefaultState();
		this.fillerBlock = ObjectHandler.stoneClay.getDefaultState();
		this.setTemperatureRainfall(1.8F, 0.2F);
		this.setHeight(new BiomeGenBase.Height(0.25F, 0.0125F));
		this.spawnableMonsterList.clear();
        this.spawnableMonsterList.add(new BiomeGenBase.SpawnListEntry(EntityCreeper.class, 100, 3, 7));
        this.spawnableMonsterList.add(new BiomeGenBase.SpawnListEntry(EntityEnderman.class, 100, 4, 4));
        this.spawnableMonsterList.add(new BiomeGenBase.SpawnListEntry(EntitySpider.class, 100, 4, 6));
	}
}
