package me.neon.refinedscience.tile;

import java.util.ArrayList;

import me.neon.refinedscience.lib.Utils;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class TilePipe extends TileTank {

	public TilePipe() {
		super(1);
	}
	
	private boolean autoEject = true;
	private boolean[] connect = new boolean[] {true, true, true, true, true, true};
	public int color = -1;

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		autoEject = nbt.getBoolean("Eject");
		color = nbt.getInteger("Color");
		//TODO: FIX
		/*NBTTagList sides = nbt.getTagList("NoConnect", nbt.getId());
		for (int c = 0; c < sides.tagCount(); c++) {
			NBTTagCompound s = sides.getCompoundTagAt(c);
			if(s != null){
				if (!noConnect.contains(EnumFacing.valueOf(s.getString("Side")))) {
					noConnect.set(c, EnumFacing.valueOf(s.getString("Side")));
				}
			}
		}*/
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setBoolean("Eject", autoEject);
		nbt.setInteger("Color", color);
		//TODO: FIX
		/*NBTTagList sides = new NBTTagList();
		for (int c = 0; c < noConnect.size(); c++) {
			NBTTagCompound s = new NBTTagCompound();
			String cs = noConnect.get(c).toString();
			if(cs != null){
				s.setString("Side", cs);
			}
			sides.appendTag(s);
		}
		nbt.setTag("NoConnect", sides);*/
	}

	public ArrayList<EnumFacing> getConnectedSides() {
		ArrayList<EnumFacing> connect = Utils.getPipeConnection(worldObj, this.getBlockType().getDefaultState(), pos);
		return connect;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(pos, new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1));
	}

	/*@Override
	public void update() {
		super.update();
		int neg = 10;
		TilePipe local = (TilePipe) worldObj.getTileEntity(pos);
		/*ArrayList<EnumFacing> neighbors = this.getConnectedSides();
		//TODO FIX;
		if (getPipeWithMinimalLiquid() != null){
			if (local.getFluidTank().getFluidAmount() > 0 && (local.getFluidTank().getFluid().isFluidEqual(getPipeWithMinimalLiquid().getFluidTank().getFluid()) || getPipeWithMinimalLiquid().getFluidTank().getFluidAmount() <= 0)) {
				if (local.getFluidTank().getFluidAmount() > getPipeWithMinimalLiquid().getFluidTank().getFluidAmount()) {
					getPipeWithMinimalLiquid().getFluidTank().fill(new FluidStack(local.getFluidTank().getFluid().getFluid(), neg), true);
					local.getFluidTank().drain(neg, true);
				}
			}
		}
	}*/

	private TilePipe getPipeWithMinimalLiquid() {
		ArrayList<EnumFacing> neighbors = this.getConnectedSides();
		TileEntity[] teN = new TileEntity[neighbors.size()];
		int min = 1000;
		int n = 0;
		int[] amtN = new int[neighbors.size()];
		for (int i = 0; i < neighbors.size(); i++) {
			teN[i] = (TilePipe) worldObj.getTileEntity(new BlockPos(pos.getX() + neighbors.get(i).getFrontOffsetX(), pos.getY() + neighbors.get(i).getFrontOffsetY(), pos.getZ() + neighbors.get(i).getFrontOffsetZ()));
			amtN[i] = ((TilePipe) teN[i]).getFluidTank().getFluidAmount();

			min = Math.min(min, amtN[i]);
			if(min == amtN[i]){
				n = i;
			}
		}
		if(neighbors.size() != 0){
			return (TilePipe) teN[n];
		}
		return null;
	}

	@Override
	public void update() {
		
	}

}
