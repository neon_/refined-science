package me.neon.refinedscience.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fluids.FluidTank;

public class TileSteamPump extends TileTank{

	private FluidTank fuel = new FluidTank(3000);
	//TODO: MAKE
	public TileSteamPump() {
		super(5);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		fuel.readFromNBT(nbt);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		fuel.writeToNBT(nbt);
	}
	
	public FluidTank getFuelTank() {
		return this.fuel;
	}
	
	@Override
	public void update(){}
}
