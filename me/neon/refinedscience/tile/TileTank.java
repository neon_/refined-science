package me.neon.refinedscience.tile;

import me.neon.nlib.tile.NTileEntity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fluids.FluidTank;

public abstract class TileTank extends NTileEntity{
	
	private FluidTank ft;
	
	public TileTank(int buckets){
		ft = new FluidTank(buckets*1000);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		ft.readFromNBT(nbt);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		ft.writeToNBT(nbt);
	}
	
	public FluidTank getFluidTank() {
		return this.ft;
	}
	
}
