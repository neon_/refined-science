package me.neon.refinedscience.tile;

import me.neon.nlib.tile.NMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.gui.IUpdatePlayerListBox;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidTank;

public class TileSteamSmasher extends NMachine implements ISidedInventory, IFluidTank {

	protected int slots = 4;
	protected int maxLiquid = 3000;
	protected FluidTank flTank;
	protected ItemStack[] items;

	public TileSteamSmasher() {
		items = new ItemStack[slots];
		flTank = new FluidTank(maxLiquid);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		flTank.readFromNBT(nbt);

		NBTTagList nbttaglist = nbt.getTagList("Items", 10);
		this.items = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i) {
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");
			if (b0 >= 0 && b0 < this.items.length) {
				this.items[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		flTank.writeToNBT(nbt);

		NBTTagList list = new NBTTagList();

		for (int i = 0; i < this.items.length; ++i) {
			if (this.items[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.items[i].writeToNBT(nbttagcompound1);
				list.appendTag(nbttagcompound1);
			}
		}

		nbt.setTag("Items", list);
	}

	@Override
	public int getSizeInventory() {
		return items.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return items[slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int size) {
		if (this.items[slot] != null) {
			ItemStack itemstack;
			if (this.items[slot].stackSize <= size) {
				itemstack = this.items[slot];
				this.items[slot] = null;
				return itemstack;
			} else {
				itemstack = this.items[slot].splitStack(size);
				if (this.items[slot].stackSize == 0) {
					this.items[slot] = null;
				}
				return itemstack;
			}
		} else {
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		if (this.items[slot] != null) {
			ItemStack itemstack = this.items[slot];
			this.items[slot] = null;
			return itemstack;
		} else {
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack is) {
		this.items[slot] = is;

		if (is != null && is.stackSize > this.getInventoryStackLimit()) {
			is.stackSize = this.getInventoryStackLimit();
		}
	}
	
	@Override
	public void update() {
		
	}

	@Override
	public String getName() {
		return this.isNameLocalized() ? tileName : StatCollector.translateToLocal("tile.steamSmasher.name");
	}

	@Override
	public boolean hasCustomName() {
		return this.isNameLocalized();
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(pos) == this && player.getDistanceSq((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D) < 64.0D && (player.getUniqueID().toString().equals(this.getOwner()) || this.hasUser(player.getUniqueID().toString()));
	}

	// TODO: Fix .DEFAULT-s
	@Override
	public boolean isItemValidForSlot(int slot, ItemStack is) {
		return true;
	}

	@Override
	public FluidStack getFluid() {
		return flTank.getFluid();
	}

	@Override
	public int getFluidAmount() {
		return flTank.getFluidAmount();
	}

	@Override
	public int getCapacity() {
		return flTank.getCapacity();
	}

	@Override
	public FluidTankInfo getInfo() {
		return flTank.getInfo();
	}

	// .DEFAULT
	@Override
	public int fill(FluidStack resource, boolean doFill) {
		return flTank.fill(resource, doFill);
	}

	// .DEFAULT
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain) {
		return flTank.drain(maxDrain, doDrain);
	}

	@Override
	public void openInventory(EntityPlayer playerIn) {}

	@Override
	public void closeInventory(EntityPlayer playerIn) {}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {

	}

	@Override
	public IChatComponent getDisplayName() {
		return null;
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		return null;
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		return false;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		return false;
	}

}
