package me.neon.refinedscience.network;

import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class CommonProxy implements IGuiHandler {
	
	public void initRenderers() {}

	public void initSounds() {}

	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile_entity = world.getTileEntity(new BlockPos(x, y, z));
		return null;
	}

	public Object getClientGuiElement(int guiID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}

	public World getClientWorld() {
		return null;
	}
}
