package me.neon.refinedscience.network;

import me.neon.refinedscience.lib.IdList;
import me.neon.refinedscience.tile.TilePipe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {
	
	public void initRenderers() {
	}

	public void initSounds() {
		
	}

	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile_entity = world.getTileEntity(new BlockPos(x, y, z));
		return null;
	}

	public World getClientWorld() {
		return FMLClientHandler.instance().getClient().theWorld;
	}
}
