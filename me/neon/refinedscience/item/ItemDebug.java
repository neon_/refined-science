package me.neon.refinedscience.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import me.neon.nlib.item.NItem;
import me.neon.refinedscience.tile.TileTank;

public class ItemDebug extends NItem {

	public ItemDebug(String uname) {
		super(uname);
		this.setMaxDamage(1);
		this.setMaxStackSize(1);
	}

	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ){
        return false;
    }
	
	@Override
	public EnumRarity getRarity(ItemStack is) {
		return EnumRarity.EPIC;
	}
}
