package me.neon.refinedscience.block.machine;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import me.neon.nlib.block.NContainer;
import me.neon.refinedscience.lib.Utils;
import me.neon.refinedscience.tile.TilePipe;
import me.neon.refinedscience.tile.TileSteamPump;

public class BlockSteamPump extends NContainer{
	//TODO: MAKE
	public BlockSteamPump(String name) {
		super(name, Material.iron);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int flag) {
		return new TileSteamPump();
	}

}
