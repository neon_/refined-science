package me.neon.refinedscience.block.machine;

import java.util.ArrayList;
import java.util.List;

import me.neon.nlib.block.NContainer;
import me.neon.refinedscience.lib.CustomTabs;
import me.neon.refinedscience.lib.Utils;
import me.neon.refinedscience.tile.TilePipe;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;

public class BlockFluidPipe extends NContainer {
	
	public BlockFluidPipe(String uname, Material mat) {
		super(uname, mat);
		this.setStepSound(soundTypeGlass);
		this.setCreativeTab(CustomTabs.tabFunction);
		this.setLightOpacity(0);
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public void setBlockBoundsForItemRender() {
		this.setBlockBounds(0.4F, 0.4F, 0.4F, 0.6F, 0.6F, 0.6F);
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, BlockPos pos) {
		ArrayList<EnumFacing> connect = ((TilePipe) world.getTileEntity(pos)).getConnectedSides();
		//Utils.removeFromList(connect, ((TilePipe) world.getTileEntity(pos)).noConnect);
		float x0 = connect.contains(EnumFacing.WEST) ? 0 : 0.4F;
		float x1 = connect.contains(EnumFacing.EAST) ? 1 : 0.6F;
		float y0 = connect.contains(EnumFacing.DOWN) ? 0 : 0.4F;
		float y1 = connect.contains(EnumFacing.UP) ? 1 : 0.6F;
		float z0 = connect.contains(EnumFacing.NORTH) ? 0 : 0.4F;
		float z1 = connect.contains(EnumFacing.SOUTH) ? 1 : 0.6F;
		setBlockBounds(x0, y0, z0, x1, y1, z1);
	}

	@Override
    public void addCollisionBoxesToList(World worldIn, BlockPos pos, IBlockState state, AxisAlignedBB mask, List list, Entity collidingEntity) {
		this.setBlockBoundsBasedOnState(worldIn, pos);
		super.addCollisionBoxesToList(worldIn, pos, state, mask, list, collidingEntity);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int flag) {
		return new TilePipe();
	}

	@Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer p, EnumFacing side, float hitX, float hitY, float hitZ) {
		ItemStack current = p.inventory.getCurrentItem();
		TileEntity tile = world.getTileEntity(pos);
		
		if (current != null) {
			FluidStack liquid = FluidContainerRegistry.getFluidForFilledItem(current);

			if (tile instanceof TilePipe) {
				TilePipe tank = (TilePipe) tile;
				if (liquid != null) {
					if(((TilePipe) tile).getFluidTank().getFluidAmount() <= 0){
						int qty = tank.getFluidTank().fill(liquid, true);
						
						if (qty != 0 && !p.capabilities.isCreativeMode) {
							p.inventory.setInventorySlotContents(p.inventory.currentItem, Utils.consumeItem(current));
						}
						return true;
					}
					return false;
				} else {
					FluidStack available = tank.getFluidTank().getFluid();

					if (available != null) {
						ItemStack filled = FluidContainerRegistry.fillFluidContainer(available, current);

						liquid = FluidContainerRegistry.getFluidForFilledItem(filled);

						if (liquid != null) {
							if (!p.capabilities.isCreativeMode) {
								if (current.stackSize > 1) {
									if (!p.inventory.addItemStackToInventory(filled)) {
										return false;
									} else {
										p.inventory.setInventorySlotContents(p.inventory.currentItem, Utils.consumeItem(current));
									}
								} else {
									p.inventory.setInventorySlotContents(p.inventory.currentItem, Utils.consumeItem(current));
									p.inventory.setInventorySlotContents(p.inventory.currentItem, filled);
								}
							}
							tank.getFluidTank().drain(liquid.amount, true);
							return true;
						}
					}
				}
			}
		}else{
			//TODO: ADD
			/*if(((TilePipe) tile).noConnect.contains(EnumFacing.UP)){
				((TilePipe) tile).noConnect.remove(EnumFacing.UP);
			}else{
				((TilePipe) tile).noConnect.add(EnumFacing.UP);
			}*/
		}

		return false;
	}

	@Override
	public int getLightValue(IBlockAccess world, BlockPos pos) {
		TileEntity tile = world.getTileEntity(pos);
		if (tile instanceof TilePipe) {
			TilePipe tank = (TilePipe) tile;
			if(tank.getFluidTank().getFluidAmount()>0){
				return tank.getFluidTank().getFluid().getFluid().getLuminosity();
			}
		}

		return super.getLightValue(world, pos);
	}

}